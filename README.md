# Ansible SSH Port Change

Taken from [https://dmsimard.com/2016/03/15/changing-the-ssh-port-with-ansible/](https://dmsimard.com/2016/03/15/changing-the-ssh-port-with-ansible/) and put in to a simple role.

Slight modification: `become: yes` is used in the individual tasks/handlers meaning we don't need to give it for the entire playbook. This solves an issue where the sudo password for two systems was required (though only one can be given).

Note: this doesn't work if you need to change the port number again, to something different or back to 22, after the initial change. I will try to add the feature soon..

Example inventory file:

    [test]
    test.local    ansible_port=[your-new-ssh-port]

An example playbook to run this role:

    ---
    
    - hosts: test
      gather_facts: no 
    
      tasks:
        - import_role:
            name: ansible-ssh-port-change

Note: `gather_facts: no` because it will try to use the new port number to connect, which will fail.

Running the playbook you may need to have

    ansible-playbook [your-playbook].yml -K

`-K` because needs the local sudo password
